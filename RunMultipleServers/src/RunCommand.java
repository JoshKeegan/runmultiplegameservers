/*
 * RunMultipleServers
 * RunCommand functions
 * By Josh Keegan 14/09/2013
 */

import java.io.*;
import java.util.*;


public class RunCommand
{
	private static final String LOG_FILE = "server.log";
	
	public static void exec(String cmd, String workingDirectory) throws IOException
	{
		String[] params = cmd.split("\\s+");
		
		List<String> liParams = new ArrayList<String>();
		
		for(String p : params)
		{
			liParams.add(p);
		}
		
		exec(liParams, workingDirectory);
	}
	
	public static void exec(List<String> params, String workingDirectory) throws IOException
	{
		ProcessBuilder pb = new ProcessBuilder(params);
		pb.redirectErrorStream(true);
		
		//If we had a working directory supplied, use it
		if(workingDirectory != null && !workingDirectory.equals(""))
		{
			pb.directory(new java.io.File(workingDirectory));
		}
		
		Process process = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		
		String line;
		while((line = reader.readLine()) != null)
		{
			System.out.println(line);
			PrintWriter out = new PrintWriter(new FileWriter(LOG_FILE, true));
			out.println(line);
			out.close();
		}
		reader.close();
	}
}
