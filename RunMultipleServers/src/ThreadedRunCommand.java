import java.io.IOException;

/*
 * RunMultipleServers
 * ThreadedRunCommand class
 * By Josh Keegan 14/09/2013
 */

public class ThreadedRunCommand implements Runnable
{
	private String command;
	private String workingDirectory;
	
	public ThreadedRunCommand(final String command, final String workingDirectory)
	{
		this.command = command;
		this.workingDirectory = workingDirectory;
	}

	@Override
	public void run()
	{
		//Cannot throw the exception when in another thread
		try
		{
			RunCommand.exec(command, workingDirectory);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			
			//TODO: Log the exception
		}
	}
	
}
