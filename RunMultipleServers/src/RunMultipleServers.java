/*
 * RunMultipleServers
 * Program to run multiple servers
 * By Josh Keegan 14/09/2013
 */

import java.util.Map.*;

public class RunMultipleServers
{
	private static final String COMMANDS_FILE = "commands.cfg";
	
	public static void main(String[] args) throws Exception
	{
		Settings settings = new Settings(COMMANDS_FILE);
		
		for(Entry<String, String> entry : settings.getCommands().entrySet())
		{
			String command = entry.getKey();
			String workingDirectory = entry.getValue();
			
			ThreadedRunCommand trc = new ThreadedRunCommand(command, workingDirectory);
			Thread thread = new Thread(trc);
			thread.start();
		}
	}
}
