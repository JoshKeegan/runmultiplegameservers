/*
 * RunMultipleServer
 * Settings class
 * By Josh Keegan
 */

import java.io.*;
import java.util.*;

public class Settings
{
	//Private vars	
	private Map<String, String> commands;
	
	//Accessors
	public Map<String, String> getCommands()
	{
		return commands;
	}
	
	public Settings(final String commandsFile) throws Exception
	{
		//Read in the commands to be run
		FileReader fileReader = new FileReader(commandsFile);
		BufferedReader reader = new BufferedReader(fileReader);
		
		String line = reader.readLine();
		commands = new HashMap<String, String>();
		while(line != null)
		{
			//Get the command
			String command = line;
			
			line = reader.readLine();
			String workingDirectory = "";
			if(line != null)
			{
				workingDirectory = line;
			}
			
			commands.put(command, workingDirectory);
			
			line = reader.readLine();
		}
		
		reader.close();
	}
}
